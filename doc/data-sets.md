# Data Sets

Several different data sets are available. This section will discuss the
CSV formats and C exposures of these different data sets.

In our CSV files, the value `-` represents "No Data" (unless stated otherwise).

## person

The person data set consists of information related to people. It has
the following CSV files:

* person/given_names.csv
    * Possible given names of a person
* person/prefixes.csv
    * Possible prefixes of a person
* person/suffixes.csv
    * Possible suffixes of a person
* person/surnames.csv
    * Possible surnames of a person

It also has the following C methods:

```c
const FakerDataPersonNamePieceEntry *faker_data_given_names(size_t *outNumEntries);

const FakerDataTextEntry *faker_data_surnames(size_t *outNumEntries);

const FakerDataTextEntry *faker_data_suffixes(size_t *outNumEntries);

const FakerDataPersonNamePieceEntry *faker_data_prefixes(size_t *outNumEntries);
```

## financial

The financial data set consists of information related to different financial
institutions and concepts (e.g. Credit Card details, currencies, etc.).

The financial data set consists of the following CSV files:

* financial/account_names.csv
    * Possible account names (with multiple languages)
* financial/cc_networks.csv
    * Credit Card network details, and details about credit cards issued by those network
* financial/currencies.csv
    * Global currency information
* financial/transaction_types.csv
    * Possible transaction types; English only

It also has the following C methods:

```c
const FakerDataTextEntry *faker_data_account_names(size_t *outNumEntries);

const FakerDataStringEntry *faker_data_transaction_types(size_t *outNumEntries);

const FakerDataCreditCardNetwork *faker_data_cc_networks(size_t *outNumEntries);

const FakerDataCurrency *faker_data_currencies(size_t *outNumEntries);
```

### CSV Notes

* financial/cc_networks.csv has arrays of values for the columns `starts` and `lengths`
    * Need to split on `|` to properly parse these files

## malicious

This data set is for values which are aimed to cause programs to misbehave.
This includes overflows, underflows, bad encodings, injection attacks, etc.

> **Important!** This data set is meant for basic testing against systems that
> you are developing! Do NOT run this against systems where you do not have
> permission to perform these sorts of attacks and malicious inputs! Do NOT
> run these in production environments without proper safeguards! Many of
> these examples are potent, meaning they can (and will in insecure software)
> cause actual damage (e.g. dropping database tables, deleting records,
> shutting down servers, performing remote code execution, etc.) Be very
> careful with how you use this data set!

This data set includes the following CSV files:

* malicious/edge_cases.csv
    * Common edge cases which tend to crop up in production systems, usually by accident. These edge cases can cause program misbehavior without proper best practices and coding guidelines.
* malicious/format_injection.csv
    * Formatting injection attacks for various languages (`printf`, Python's format, Log4Shell, etc). Many of these try to perform memory dumps or remote code execution
* malicious/sql_injection.csv
    * A bunch of SQL injection examples. These are the most potent of the attacks. They can drop tables, delete or corrupt data, shut down servers, leak contents of sensitive files, etc.
* malicious/xss.csv
    * Several XSS injection attacks. Will try to do URL encoding, JS encoding, HTML encoding, attribute escaping, recursive tags, etc. Some examples do try to do remote code execution.

This data set includes the following C methods:

```c
const FakerDataStringEntry *faker_data_sql_injection(size_t *outNumEntries);

const FakerDataStringEntry *faker_data_xss(size_t *outNumEntries);

const FakerDataStringEntry *faker_data_format_injection(size_t *outNumEntries);

const FakerDataLengthStringEntry *faker_data_edge_cases(size_t *outNumEntries);
```

### CSV Notes

* malicious/edge_cases.csv uses hexadecimal byte encoding for the `bytes` column

### C Notes

* `faker_data_edge_cases` will return strings whose length cannot be determined with `strlen`. Because of this, you will need to rely on the `len` property which is part of the `FakerDataLengthStringEntry` struct.

## World

This data set is information about the world.

It includes the following CSV files:

* world/countries.csv
    * A list of countries and their ISO codes (Alpha2, Alpha3, Numeric), GENC codes, Stanag codes, and top level domains

It also includes the following C functions:

```c
const FakerDataCountry *faker_data_countries(size_t *outNumEntries);
```

### CSV Notes

* world/countries.csv